---
layout: comparison_page
title: GitLab Compared to other tools
suppress_header: true
extra_css:
  - compared.css
---

## GitLab CE vs. GitLab EE

To learn more about how GitLab Community Edition compares to GitLab Enterprise Edition, take a look at the [comparison table on our features page][comparison-versions].

[comparison-versions]: https://about.gitlab.com/features/#compare