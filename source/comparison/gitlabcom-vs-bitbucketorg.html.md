---
layout: comparison_page
title: GitLab Compared to other tools
suppress_header: true
extra_css:
  - compared.css
---

## GitLab.com vs. Bitbucket.org

### Everything mentioned in GitLab CE/EE versus Bitbucket Server

### Unlimited collaborators
With GitLab.com, you don't have to start paying when you have more than 5 collaborators across all your private repositories.
