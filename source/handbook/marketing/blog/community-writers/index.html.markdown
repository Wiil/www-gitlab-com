---
layout: markdown_page
title: "Community Writers Program"
description: "Write for GitLab and get compesated!"
---

<br>

![Write for GitLab](/images/community/computers-table-banner.jpg)

{::options parse_block_html="true" /}

<div class="alert alert-purple center">
## <i class="fa fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Get paid to write for GitLab <i class="fa fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{:.gitlab-orange .text-center #write-for-gitlab}
</div>

All great contributions come from developers “scratching their own itch.” If you've ever
solved a challenging problem or are great a explaining complex ideas, it’s likely 
you can share some advice to help someone along the way. Contribute the technical 
content you wish was available.

When you get published on our blog **you can earn up to $200** for technical articles. 
Once accepted, you’ll get feedback on your writing, and be guided through making 
the best resources. Get started today!

## How it works

1.  Choose a topic and submit a writing sample to us
2.  Get approved
3.  Start writing, get feedback, and revise
4.  Publish
5.  Get paid

## Compensation

- In-depth tutorials or opinion pieces of 1,500+ words: **USD 200.00**
- Short tutorials of 800-1500 words: **USD 100.00**
- [Top-priority](#top-priority): **USD 200.00**

Post with less than 800 words, as articles on quick tips and feature highlights, are also very welcome, but will not be compensated for.

## What we're looking for

We’re inviting community contribution so we can expand the range of tutorials and advice about creating, collaborating, and deploying with GitLab.

It's important that the content is:

- Accurate
- Complete
- Original

## Topics

To find out what topics we're looking for, review the blog post backlog and see if there are any existing requests for topics that inspire you. 
You can either [look for labeled issues](#look-for-labeled-posts) or [file a new one][topics-issues].

### Look for Labeled Posts

Navigate to the [Blog Posts issue tracker][topics-issues] and look for issues [labeled](../#labels) with:

- [Community Posts]
- [Up-for-grabs]
- [$100]
- [$200]
- [TOP PRIORITY]

<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: rgb(49, 112, 143);"></i>&nbsp;
Issues labeled **TOP PRIORITY**, will be compenstated at **USD 200** regardless of length.
{:.alert .alert-info .text-center #top-priority}

## Writing Process

We have some stantards, so please, make sure you've **read them before submitting your proposal**. 
The technical process of writing and reviewing can be found in our Handbooks: [GitLab Blog] and [Technical Writing].

## Publishing Process

Read through the [Publishing Process for Community Writers](../#publishing-process-for-community-writers).

## Get Paid

When your post gets published, it's time to claim for your compensation. You will send us an invoice, GitLab will pay you in American Dollars (USD) from a bank account in the USA, via wired transfer to your bank account. We can deal with other possible payment methods via credit card, but please discuss it with us before start writing.

## Important Notes and Exceptions

- If you want to write about your own product (or the product you represent), and how it integrates with GitLab, we'll be happy to have you as a [Guest Writer](../#guest-posts). The Community Writers Program won't apply for these cases.
- Once your community post gets published, we'll count on you for providing updates for the process you wrote about, for one year from the publishing date. We won't compensate you for updates.
- You are free to offer yourself to write more than one post. If you succeed on the first process, we'll be glad to have you writing for us regularly. The process for writing more than one post is exactly the same for writing the first one.
- The review we provide is necessary. You're free to discuss and make your point, but you need to take into account the experience of our reviewers and the QA standards they will require from you.
- Your post needs to be approved by the Blog Editorial Team to get published.
- Your post must be original, comprehensible, technical, and unprecedented. We won't accept non-original content, unless you want to publish your post as a [Crosspost](../#crossposts), which we don't compensate for.

_**Note:** When you start writing for GitLab, we assume you have accepted the terms described along this page._

<!-- identifiers -->

[CI/CD/CD]: /2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/
[ConvDev]: /2016/09/13/gitlab-master-plan/#convdev
[GitLab Blog]: /handbook/marketing/blog/#publishing-process-for-community-writers
[our blog]: /blog/
[Pages group]: https://gitlab.com/groups/pages
[Technical Writing]: /handbook/marketing/developer-relations/technical-writing/#professional-writing-techniques
[topics-issues]: https://gitlab.com/gitlab-com/blog-posts/issues/

<!-- labels -->

[Community Posts]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=Community+Posts
[Up-for-grabs]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=up-for-grabs
[$100]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=%24+100
[$200]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=%24200
[TOP PRIORITY]: https://gitlab.com/gitlab-com/blog-posts/issues?label_name%5B%5D=TOP+PRIORITY

<style>
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
.alert-purple h2 {
      margin-top: 15px;
}
</style>
